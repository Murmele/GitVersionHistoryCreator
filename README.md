# GitVersionHistoryCreator
Creating a Latex Git History

# Features
- Reading git commit messages and storing in a latex file within an itemize environment
- Reading already existing version history and appending new changes
