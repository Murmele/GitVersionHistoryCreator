from importExportHistory import parseVersionHistoryFile, writeVersionHistoryFile
from versionHistoryCreation import createVersionHistory
import re

if __name__ == "__main__":

    # Reading in the already existing history.
    history = parseVersionHistoryFile("exampleFiles", "VersionHistory.tex")

    repository_path = "."
    subfolder_path = repository_path # if you would like to track another subfolder, change this variable
    tagRegex = re.compile("Device_R(?P<Revision>[0-9]*)")
    currRevision = "6"
    historyExportSuccess = createVersionHistory(repository_path, subfolder_path,
                                                tagRegex, history, currRevision=currRevision)

    writeVersionHistoryFile(".", history, export_filename="generated/VersionHistory.tex")