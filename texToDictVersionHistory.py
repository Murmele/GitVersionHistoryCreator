from pylatexenc.latexwalker import LatexWalker, LatexEnvironmentNode, LatexMacroNode, LatexCharsNode

"""
Reading a texfile an creates the version history. So the file gets appended and not completely rewritten every time.
So it is possible to manually edit the version history file if the content is not fine.
"""

def cleanLine(line):
    return line.replace("\t", "").replace("\n", "").replace("\\textunderscore{}", "_")

def parseItems(itemizeNode, recursive=False):
    """
    Parse only the items, no recursive parsing!
    Args:
        itemizeNode:

    Returns:

    """
    items = []
    if type(itemizeNode) != LatexEnvironmentNode or itemizeNode.envname != "itemize":
        return items

    # states = ["searchItem", "parseItem"]

    state = "searchItem"
    itm = {}
    itm["value"] = ""
    itm["childs"] = []
    for item in itemizeNode.nodelist:
        if state == "searchItem" and type(item) == LatexMacroNode and item.macroname == "item":
            # if line:
            #     items.append(line)
            state = "parseItem"
        elif state == "parseItem" and type(item) == LatexMacroNode and item.macroname == "item":
            # new item found
            # add the other one to the items
            items.append(itm)
            itm = {}
            itm["value"] = ""
            itm["childs"] = []
            state = "parseItem"
        elif state == "parseItem":
            if type(item) == LatexCharsNode:
                itm["value"] += cleanLine(item.chars)
            elif type(item) == LatexMacroNode:
                macro = "\\" + item.macroname + "{}"
                itm["value"] += cleanLine(macro)
            elif type(item) == LatexEnvironmentNode and itemizeNode.envname == "itemize" and recursive:
                itm["childs"] = parseItems(item, recursive)
        else:
            state = "searchItem"

    items.append(itm)
    return items



def parseTexVersionHistory(tex_code):
    history = []
    w = LatexWalker(tex_code)
    (nodelist, pos, len_) = w.get_latex_nodes(pos=0)
    items = []
    for node in nodelist:
        if type(node) == LatexEnvironmentNode and node.envname == "itemize":
            # found version history itemize environment
           items = parseItems(node, recursive=True)

    # ignore all childs from the childs, they make no sense yet
    version = {}
    version["changes"] = []
    for revision in items:
        version["revision"] = revision["value"]
        for changes in revision["childs"]:
            version["changes"].append(changes["value"])
        history.append(version)
        version = {}
        version["changes"] = []

    return history

if __name__ == "__main__":
    code = r"""
    \begin{itemize}
			\item version 1
			\begin{itemize}
				\item first change of version 1
				\item second change of version 1
			\end{itemize}
			\item version 2
			\begin{itemize}
				\item first change of version 2
				\item second change of version 2
			\end{itemize}
		\end{itemize}
    """
    history = parseTexVersionHistory(code)
    print(history)