"""
Creates a latex itemize environment, which contains all the history logs
"""
def createItem(text):
    return "\\item " + text

def createLatexText(text):
    return text.replace("_", "\\textunderscore{}").replace("^", "$\hat{}$")

def createItemize(content):
    if type(content) is str:
        return "\\begin{itemize}\n" + content + "\n" + "\\end{itemize}\n"
    if type(content) is list:
        items = ""
        for i in range(len(content)):
            element = createLatexText(content[i])
            items += createItem(element)
            if i < len(content) - 1:
                items += "\n"
        if items:
            return createItemize(items)
    return ""

def historyListToTex(history):
    if not history:
        return ""
    content = ""
    for entry in history:
        if "revision" not in entry or "changes" not in entry:
            print(f"Not a valid history entry: {entry}")
        content += createItem(createLatexText(entry["revision"])) + "\n"
        content += createItemize(entry["changes"])

    if content:
        return createItemize(content)
    return ""  # if no history is available, just return

if __name__ == "__main__":
    history = [{'revision': 'version 1', 'changes': ['first change of version 1', 'second change of version 1']},
                {'revision': 'version 2', 'changes': ['first change of version 2', 'second change of version 2']}]

    print(historyListToTex(history))