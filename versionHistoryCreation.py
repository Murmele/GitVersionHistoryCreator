import git
import os
import re
import pathlib
import subprocess
from shlex import quote

"""
Creates the version history from the git commits and the tags.
"""

# TODO: get all alias names when renaming a file and check these tags too
# git log --name-status --pretty="%s" --oneline -M90%

def getLogs(repository_path, subfolderPath, startTag, endTag):
    g = git.Git(repository_path)
    # # git log --oneline --graph "FuselageRear_R2.0.1..FuselageRear_R2.1.0" "WH-VB2__Wing_Inner_Doc.docx"
    # # , '--pretty="%b"'
    # quote is important
    modLogs = []
    logs = g.log('--oneline', '--pretty="%s"', '--follow' ,quote(f"{startTag}..{endTag}"), subfolderPath.as_posix()).split("\n")

    # works also
    # cmd = ['git', 'log', '--oneline', '--pretty="%s"', quote(f"{startTag}..{endTag}"), subfolderPath.as_posix()]
    # try:
    #     logs = subprocess.check_output(
    #         cmd, stderr=subprocess.STDOUT, shell=False, timeout=3,
    #         universal_newlines=True, cwd=repository_path).split("\n")
    # except subprocess.CalledProcessError as exc:
    #     print("Status : FAIL", exc.returncode, exc.output)
    #     return None

    for log in logs:
        log = log[1:-1] # remove quotes
        modLogs.append(log)
    return modLogs

def getCurrentRevisionLogs(repository_path, subfolderPath, startTag):
    repo = git.Repo(repository_path)
    g = git.Git(repository_path)
    heads = repo.heads
    master = heads.master  # lists can be accessed by name for convenience
    index_commit_id = str(master.commit)  # the commit pointed to by head called master

    modLogs = []
    logs = g.log('--oneline', '--pretty="%s"', '--follow', quote(f"{startTag}..{index_commit_id}"),
                 subfolderPath.as_posix()).split("\n")

    for log in logs:
        log = log[1:-1] # remove quotes
        modLogs.append(log)
    return modLogs

def getInitialChanges(repository_path, subfolderPath):
    repo = git.Repo(repository_path)
    g = git.Git(repository_path)
    heads = repo.heads
    master = heads.master  # lists can be accessed by name for convenience
    index_commit_id = str(master.commit)  # the commit pointed to by head called master

    modLogs = []
    logs = g.log('--oneline', '--pretty="%s"', '--follow', quote(f"{index_commit_id}"),
                 subfolderPath.as_posix()).split("\n")

    for log in logs:
        log = log[1:-1] # remove quotes
        modLogs.append(log)
    return modLogs

def revision_greater(rev1, rev2):
    # rev1 > rev2
    r1 = rev1.split(".")
    r2 = rev2.split(".")

    for i in range(len(r1)):
        if len(r2) > i:
            if int(r1[i]) > int(r2[i]):
                return True
            if int(r1[i]) < int(r2[i]):
                return False
        else:
            return False

    return False

def sortTags(tags):
    # from lower to higher version
    sortedTags = []
    for tag in tags:
        added = False
        for i in range(len(sortedTags)):
            if revision_greater(sortedTags[i]["revision"], tag["revision"]):
                sortedTags.insert(i, tag)
                added = True
                break
        if not added:
            sortedTags.append(tag)

    return sortedTags

def determineRevisionGroup(pattern):
    """
    This function determines the pattern for the Revision group
    Args:
        pattern:

    Returns:

    """
    prefix = "(?P<Revision>"
    index = pattern.find(prefix)
    if index < 0:
        return None

    brackets = 0
    for i in range(index, len(pattern)):
        if pattern[i] == "(":
            brackets += 1
        elif pattern[i] == ")":
            brackets -= 1

        if brackets == 0:
            return pattern[index:i+1]
    return None  # non valid group available

def versionEqual(v1, v2):
    v1_split = v1.split(".")
    v2_split = v2.split(".")

    if v1 == v2:
        return True

    # example 3 == 3.0. Ignore zero
    if (len(v1_split) == 1 and v2_split[0] == v1_split[0] and v2_split[1] == "0") or \
        (len(v2_split) == 1 and v1_split[0] == v2_split[0] and v1_split[1] == "0"):
        return True
    return False

def createVersionHistory(repository_path, subfolder, tagRegex, existingHistory=[], currRevision=None):
    """

    Args:
        repository_path:
        subfolder:
        tagRegex:
        existingHistory:
            list of dicts. Each dict contains the key revision with a string and a changes key with a list of changes
            example:
                [{'revision': 'WingInner_R2.0.0', 'changes': ['Initial release']},
                {'revision': 'WingInner_R2.0.1', 'changes': ['WH-VB2__Wing_Inner: replace manufacturer number in document, because it did not match with the url', 'increase version number of all tagged documents']},
                {'revision': 'WingInner_R2.0.2', 'changes': ['WH-SD1__Copter_Arm, WH-SD3__Copter_Rear, WH-VA3__Fuselage_Rear, WH-VB2__Wing_Inner', 'WH-VB2__Wing_Inner: Increase version number']},
                {'revision': 'WingInner_R2.0.3', 'changes': ['use differnt cable chain number for rear and wing, because they are different', 'WH-SD1__Copter_Arm, WH-SD3__Copter_Rear, WH-VA3__Fuselage_Rear, WH-VB2__Wing_Inner: Increase version numbers']},
                {'revision': 'WingInner_R2.0.4', 'changes': ['WH-VB2__Wing_Inner, WH-VA3__Fuselage_Rear: change cable chain partnumbers to the ones Alex gave me. They are more sorted', 'WH-VA3__Fuselage_Rear, WH-VB2__Wing_Inner: increase version numbers']},
                {'revision': 'WingInner_R2.0.5', 'changes': ['Add comment that connectors must be wided']},
                {'revision': 'WingInner_R2.0.6', 'changes': ['WH-VB2__Wing_Inner: In the specification the MALE part number was specified. We need the female']},
                {'revision': 'WingInner_R2.1.0', 'changes': ['Move CAN wires to other pins in the wing connector']}]
        currRevision:

    Returns:

    """
    repo = git.Repo(repository_path)
    matchingTags = []
    for tag in repo.tags:
        match = tagRegex.match(str(tag))
        if match is not None:
            if "Revision" not in match.re.groupindex:
                print("Revision not found in regular expression.")
                return False
            matchingTags.append({"tag": tag, "revision": match["Revision"]})



    currRevisionTag = None
    if currRevision is not None:
        revGroup = determineRevisionGroup(tagRegex.pattern)
        if not revGroup:
            print("Revision group is not correct.")
            return False
        currRevisionTag = str(tagRegex.pattern).replace(revGroup, currRevision)


    subfolderRel = os.path.relpath(subfolder, repository_path)
    subfolderRel = pathlib.PureWindowsPath(subfolderRel)
    sortedTags = sortTags(matchingTags)
    for i in range(0, len(sortedTags)):
        endTag = sortedTags[i]["tag"]
        if i > 0:
            startTag = sortedTags[i - 1]["tag"]
        else:
            startTag = endTag

        found = False
        for tags in existingHistory:
            if tags["revision"] == endTag.name:
                # For that revision the changelog is already available, do not overwrite
                found = True
                break

        if found:
            continue

        if startTag == endTag:
            logs = [f"Initial release"]
        else:
            logs = getLogs(repository_path, subfolderRel, startTag=startTag, endTag=endTag)

        existingHistory.append({"revision": endTag.name, "changes": logs})

    # add the history until the current commit
    # if the tag alredy exists, add the changes as own itemize, so it can be distinguished, what was already checked
    # and what not
    if len(sortedTags) == 0 or not versionEqual(sortedTags[-1]["revision"], currRevision):
        if len(sortedTags) > 0:
            logs = getCurrentRevisionLogs(repository_path, subfolderRel, sortedTags[-1]["tag"])
        else:
            logs = getInitialChanges(repository_path, subfolderRel)
        if len(logs) > 0 and logs[0] != '':
            existing = False
            for h in existingHistory:
                if h["revision"] == currRevisionTag:
                    existing = True
                    # check if the content is still the same
                    if log1ContainsLog2(logs, h["changes"]):
                        h["changes"] = logs  # same or in logs are now more, so just update
                    else:
                        name_revision = currRevisionTag + "***"
                        exist = False
                        # replace if exists, otherwise just append to the list
                        for h in existingHistory:
                            if h["revision"] == name_revision:
                                exist = True
                                h["changes"] = logs
                                break
                        if not exist:
                            existingHistory.append({"revision": name_revision, "changes": logs})
                        break
            if not existing:
                name_revision = currRevisionTag
                existingHistory.append({"revision": name_revision, "changes": logs})
    return True

def log1ContainsLog2(logs1, logs2):
    for log2 in logs2:
        found = False
        for log1 in logs1:
            if log1 == log2:
                found = True
                break
        if not found:
            return False
    return True

if __name__ == "__main__":
    # example
    repository_path = "C:/Users/mmarmsoler/Documents/GIT/wiring"

    history = []
    createVersionHistory(repository_path, str(os.path.join(repository_path, r"Vector\VP6\WH-VB2__Wing_Inner")),
                                                           re.compile("WingInner_R(?P<Revision>([0-9]*\.){2}[0-9]*)"), existingHistory=history)
    print(history)