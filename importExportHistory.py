import os

from texToDictVersionHistory import parseTexVersionHistory
from historyListToTex import historyListToTex

versionHistoryFileTex = "VersionHistory.tex"
versionHistoryFile = "VersionHistory.txt"

def parseVersionHistoryFile(subfolderPath, versionHistoryFileName = None):
    """
    Read in the already existing version history file, because it should not
    be overwritten
    This parser can parse tex files with itemized revision/changes
    or
    text files with * before every revision and every change
    :param subfolderPath:
    :return:
    """
    history = []
    if versionHistoryFileName is None:
        file = os.path.join(subfolderPath, versionHistoryFileTex)
        if not os.path.isfile(file):
            # old txt file if tex file does not exist yet, but preferr tex file
            file = os.path.join(subfolderPath, versionHistoryFile)
    else:
        file = os.path.join(subfolderPath, versionHistoryFileName)

    if not os.path.isfile(file):
        return history

    file_ending = file.split(".")[-1]
    if file_ending == "txt":
        # old format
        with open(file) as f:
            lines = f.readlines()

        version = {}
        for line in lines:
            line = line.replace("\n", "")
            if line == "":
                pass
            elif not line.startswith("* "):
                if "revision" in version:
                    history.append(version)
                    version = {}
                version["revision"] = line
            else:
                if "revision" not in version:
                    raise Exception()
                if "changes" not in version:
                    version["changes"] = []
                # remove star in front of the change line
                if line[0] == "*":
                    if line[1] == " ":
                        line = line[2:]
                    else:
                        line = line[1:]
                version["changes"].append(line)

        if "revision" in version:
            history.append(version)
    elif file_ending == "tex":
        with open(file) as f:
            content = f.read()
        history = parseTexVersionHistory(content)

    return history

def writeVersionHistoryFile(subfolderPath, history, export_filename):

    if export_filename is None:
       export_filename = os.path.join(subfolderPath, versionHistoryFileTex)

    file_ending = export_filename.split(".")[-1]

    path, filename = os.path.split(export_filename)
    if not os.path.isdir(path):
        os.mkdir(path)

    with open(export_filename, 'w') as f:
        if file_ending == "txt":
            for version in history:
                f.write(version["revision"] + "\n")
                for changes in version["changes"]:
                    f.write("* " + changes + "\n")
                f.write('\n')
        elif file_ending == "tex":
            f.write(historyListToTex(history))